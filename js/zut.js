var FPS_60 = 1000/60;
var FPS_25 = 1000/25;


var Zut = function Zut($canvas, fps, timestep) {

	var self = this;
	var dt = timestep || FPS_60;
	var framerate = 1000 / (fps || FPS_25);
	var objects = [];
	var ctx = $canvas.getContext("2d");  // on-screen canvas

	// create in-memory canvas for double buffering
	var $mcanvas = document.createElement("canvas");
	$mcanvas.width = $canvas.width;
	$mcanvas.height = $canvas.height;
	var buffer = $mcanvas.getContext("2d");

	// game loop
	var start = 0, animFrameID = 0, accumulator = 0;

	function step(now) {
		accumulator += Math.min(framerate, now - start);

		while(accumulator >= dt) {
			self.update(dt);
			accumulator -= dt;
		}

		self.draw(buffer);

		ctx.clearRect(0, 0, $mcanvas.width, $mcanvas.height);
		ctx.drawImage($mcanvas, 0, 0);  // write buffer to screen

		animFrameID = window.requestAnimationFrame(step);
		start = now;
	}

	this.update = function noop(dt) {};
	this.draw = function noop(ctx) {};

	this.start = function() {
		start = window.performance.now();
		step(start);
	};

	this.alors = this.start;

	this.stop = function() {
		self.draw(buffer);

		ctx.clearRect(0, 0, $mcanvas.width, $mcanvas.height);
		ctx.drawImage($mcanvas, 0, 0);  // write buffer to screen
		window.cancelAnimationFrame(animFrameID);
	};

	this.add = function(obj) {
		obj.zut = self;
		objects.push(obj);
	};

	this.resources = new Zut.Loader();
};


Zut.$ = document.querySelectorAll.bind(document);  // jQuery-style selector shortcut
Zut.on = document.addEventListener.bind(document);
Zut.log = window.console;

// utility functions
Zut.each = function(arr, callback) {
	for(var i=0; i<arr.length; ++i) {
		callback.call(arr[i]);
	}
};


Zut.button = {
	LEFT: -1,
	MIDDLE: -2,
	RIGHT: -3,
	WHEELDOWN: -4,
	WHEELUP: -5
};

Zut.key = (function() {
	var pressed = {};

	function keyDown(e) {
		if (!pressed[e.keyCode]) {
			pressed[e.keyCode] = e.timeStamp;
		}
		e.preventDefault();
	}

	function keyUp(e) {
		if (pressed[e.keyCode]) {
			delete pressed[e.keyCode];
		}
		e.preventDefault();
	}

	function reset() {
		pressed = {};
	}

	// listen to key events and keep track of pressed keys
	Zut.on("keyup", keyUp, false);
	Zut.on("keydown", keyDown, false);
	Zut.on("blur", reset, false);

	return {
		LEFT: 37,
		UP: 38,
		RIGHT: 39,
		DOWN: 40,
		SPACE: 32,
		ENTER: 13, 
		ESC: 27,
		S: 83,
		Q: 81,
		A: 65,
		W: 87,

		pressed: function(keyCode) {
			return !!pressed[keyCode];
		}
	};
})();



/**
 * Image loader
 *
 * Usage:
 *	> var loader = new Zut.Loader();
 *	> loader.finished(function() {// this is called when everything is loaded});
 *	> loader.load();
 */
Zut.Loader = function() {

	var callback;
	var queue = [];
	var cache = {};
	loaded = 0;
	var errors = 0;

	function is_finished() {
		return (loaded + errors === queue.length && callback);
	}

	function success() {
		++loaded;
		if(is_finished()) return callback();
	}

	function error() {
		++errors;
		Zut.log.error("Error loading file: " + this.src);
		if(is_finished()) return callback();
	}

	/**
	 * @param path {string} path to file 
	 */
	this.add = function(path_or_array) {
		path_or_array = [].concat(path_or_array);  // "cast" to array

		for(var path, i=0; i<path_or_array.length; ++i) {
			path = path_or_array[i];
			queue.push(path);
		}
	};

	/**
	 * Set callback function
	 * @param cb {function} - callback
	 * @returns {void}
	 */
	this.finished = function(cb) {
		if (typeof cb === "function") callback = cb;
	};

	this.load = function() {
		if (queue.length < 1) return callback();
		for(var path, img, i=0; i<queue.length; ++i) {
			path = queue[i];
			img = new Image();
			img.src = path;
			img.addEventListener("load", success, false);
			img.addEventListener("error", error, false);
			cache[path] = img;
		}
	};

	this.get = function(path) {
		return cache[path];
	};
};


// Exports
window.Zut = Zut;
