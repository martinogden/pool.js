var Pool = {VERSION: '0.0.1'};

Pool.BALL_MAX_VELOCITY = 2;  // max velocity of balls
Pool.BALL_RADIUS = 16;
Pool.BALL_ACCELERATION = -0.0005;  // global acceleration of balls
Pool.AIMER_MAX_LENGTH = 180;

Pool.COLOR = {
	RED: "#DF0101",
	YLW: "#F7FE2E",
	GRN: "#31B94D",
	WTE: "#FFFFFF",
	BLK: "#111111",
	BLUA: "rgba(144, 254, 251, 0.3)",
	WTEA: "rgba(255, 255, 255, 0.4)",
	BLKA: "rgba(0, 0, 0, 0.4)",
};


Pool.Shot = function(cue_ball, obj_balls, direction, power) {

	var state = {
		cue_ball: cue_ball.getState(),
		obj_balls: [],
		direction: direction,
		power: power
	};

	Zut.each(obj_balls, function() {
		state.obj_balls.push(this.getState());
	});

	return state;
};


Pool.Aimer = function(sx, sy) {

	var ex = sx;
	var ey = sy;

	function getLength() {
		var x = sx - ex;
		var y = sy - ey;
		var l = Math.sqrt(x*x + y*y);
		return Math.min(l, Pool.AIMER_MAX_LENGTH);
	}

	this.moveTo = function(x, y) {
		ex = x;
		ey = y;
	};

	this.draw = function(ctx) {
		var dir = this.getDirection();
		var len = getLength();

		ctx.strokeStyle = Pool.COLOR.BLUA;
		ctx.lineWidth = 2*Pool.BALL_RADIUS;
		ctx.lineCap = "round";

		ctx.beginPath();
		ctx.moveTo(sx, sy);
		ctx.lineTo(sx - len*dir.x, sy - len*dir.y);
		ctx.stroke();
		ctx.closePath();
	};

	this.getPower = function() {
		return Pool.BALL_MAX_VELOCITY * getLength() / Pool.AIMER_MAX_LENGTH;
	};

	this.getDirection = function() {
		var x = sx - ex;
		var y = sy - ey;
		var l = Math.sqrt(x*x + y*y);

		return {
			x: x / l,
			y: y / l
		};
	};
};


Pool.Ball = function(id, rx, ry, r, color) {
	this.r = r || 16;  // radius
	color = color || Pool.COLOR.RED;

	this.id = id;
	// TODO remove hardcoded rem / timestep
	this.rem = FPS_60;  // time step remaining (collision detection)
	this.rx = rx;
	this.ry = ry;
	this.vx = 0;
	this.vy = 0;

	var highlight = false;

	var ax, ay;
	var px, py;

	this.setV = function(x, y) {  // set velocity
		this.vx = x;
		this.vy = y;
		ax = x * Pool.BALL_ACCELERATION;
		ay = y * Pool.BALL_ACCELERATION;
	};

	// Highlight ball in next draw
	this.highlight = function() {
		highlight = true;
	};

	this.isMoving = function() {
		return this.vx + this.vy !== 0;
	};

	this.update = function(dt) {
		if (this.rem < dt) dt = this.rem;

		px = this.vx >= 0;
		py = this.vy >= 0;

		this.vx += ax * dt;
		this.vy += ay * dt;

		// prevent acceleration changing direction of ball as v = 0
		if (px !== this.vx >= 0) this.vx = ax = 0;
		if (py !== this.vy >= 0) this.vy = ay = 0;

		this.rx += this.vx * dt;
		this.ry += this.vy * dt;

		this.rem = FPS_60;
	};

	this.draw = function(ctx) {
		if (highlight) {
			highlight = false;
			ctx.strokeStyle = Pool.COLOR.WTEA;
		} else ctx.strokeStyle = Pool.COLOR.BLKA;

		ctx.fillStyle = color;

		ctx.lineWidth = 1;

		ctx.beginPath();
		ctx.arc(this.rx, this.ry, this.r, 0,  Math.PI * 2); 
		ctx.fill();
		ctx.stroke();

		ctx.fillStyle = Pool.COLOR.WTEA;
		ctx.beginPath();
		ctx.arc(this.rx-this.r/4, this.ry-this.r/4, this.r/3, 0,  Math.PI * 2);
		ctx.fill();
	};

	this.getState = function() {
		return {
			id: this.id,
			rx: this.rx,
			ry: this.ry,
			r: this.r,
			color: color
		};
	};
};


Pool.Game = function($canvas, debug) {
	self = this;
	debug = debug || false;
	var w = $canvas.width;
	var h = $canvas.height;
	var balls = [];
	var history = [];
	var cball;
	var aimer;

	this.undo = function() {
		if (history.length < 1) return;

		var shot = history.pop();
		this.load(shot);
	};

	this.replay = function() {
		if (history.length < 1 || cball.isMoving()) return;

		var shot = history[history.length-1];

		this.load(shot);
		cball.setV(shot.direction.x * shot.power, shot.direction.y * shot.power);
	};

	this.load = function(shot) {
		balls = [];

		var cb = shot.cue_ball;
		cball = new Pool.Ball(cb.id, cb.rx, cb.ry, cb.r, cb.color);
		balls.push(cball);

		Zut.each(shot.obj_balls, function() {
			if (this.id === cball.id) return;
			balls.push(new Pool.Ball(this.id, this.rx, this.ry, this.r, this.color));
		});
	};

	function rack() {
		// cue ball
		cball = new Pool.Ball(0, w-231, h/2-Pool.BALL_RADIUS ,Pool.BALL_RADIUS, Pool.COLOR.WTE);
		balls.push(cball);

		// rack
		var d = 2*Pool.BALL_RADIUS+1, o = 0, c, k = 1;
		var x = Math.cos(Math.PI/6);
		var y = Math.sin(Math.PI/6);
		for (var i=0; i<6; ++i) {
			o = d*i*y;
			for (var j=0; j<i; ++j) {
				c = i===3&&j==1 ? Pool.COLOR.BLK : [Pool.COLOR.RED, Pool.COLOR.YLW][j%2];
				balls.push(new Pool.Ball(++k, 340 - i*d*x, h/2 + j*d - o,Pool.BALL_RADIUS, c));
			}
		}
	}

	// game loop
	var zut = new Zut($canvas);

	zut.update = function update(dt) {
		Pool.Collisions.resolve(dt, balls, w, h);  // detect and resolve collisions


		Zut.each(balls, function() {
			this.update(dt);
		});
	};

	zut.draw = function draw(ctx) {
		ctx.clearRect(0, 0, w, h);

		Zut.each(balls, function() {
			this.draw(ctx);
		});

		if (aimer) aimer.draw(ctx);
	};

	var dx, dy;
	Zut.on("mousedown", function(e) {
		// are we able to play a shot (is cue ball stationary)
		if (cball.isMoving()) return;

		// are we clicking on cue ball
		dx = e.layerX - cball.rx;
		dy = e.layerY - cball.ry;
		if (dx*dx + dy*dy > cball.r*cball.r) return;

		aimer = new Pool.Aimer(cball.rx, cball.ry);
	});

	Zut.on("mousemove", function(e) {
		if (!aimer) return;

		aimer.moveTo(e.layerX, e.layerY);
	});

	Zut.on("mouseup", function(e) {
		if (!aimer) return;

		// save position
		var direction = aimer.getDirection();
		var power = aimer.getPower();
		var shot = new Pool.Shot(cball, balls, direction, power);
		history.push(shot);

		// take shot
		cball.setV(direction.x * power, direction.y * power);
		aimer = undefined;
	});

	// DEBUG
	Zut.on("keydown", function(e) {
		if (!debug) return;

		if (e.keyCode === Zut.key.ENTER) {
			self.replay();
		}

		if (e.keyCode === Zut.key.ESC) {
			self.undo();
		}

		if (e.keyCode === Zut.key.SPACE) {
			Zut.log.debug(history[history.length - 1]);
		}
	});

	rack();
	zut.start();

	window.onerror = zut.stop;
};
