Pool.Collisions = {};

// co-efficient of restitution
Pool.Collisions.E = {
	BB: 0.8,  // between balls
	BC: 0.6   // between balls and cushion
};

Pool.Collisions.resolve = function (dt, balls, w, h) {

	var Dir = {
		N: 0,
		S: 1,
		E: 2,
		W: 3
	};


	// detection
	function ballsCollideAt(A, B) {
		var drx = B.rx - A.rx;
		var dry = B.ry - A.ry;

		var dvx = B.vx - A.vx;
		var dvy = B.vy - A.vy;

		var sr2 = Math.pow(A.r + B.r, 2);

		var r2 = drx*drx + dry*dry;
		var rv = drx*dvx + dry*dvy;
		var v2 = dvx*dvx + dvy*dvy;

		var D = 4*rv*rv - 4*v2*(r2 - sr2);
		if (D < 0) return Infinity;

		var sqrt_D = Math.sqrt(D);

		var t0 = (-2*rv - sqrt_D) / (2*v2);
		if (t0 >= 0) return t0;

		var t1 = (-2*rv + sqrt_D) / (2*v2);
		if (t1 >= 0) return t1;

		return Infinity;
	}


	function detectBB(A, nextCol) {
		var t = nextCol.at;

		for(var i=0, B; i<balls.length; ++i) {
			B = balls[i];
			if (A.id === B.id) continue;

			tp = ballsCollideAt(A, B);
			if (tp <= t && tp <= A.rem && tp <= B.rem) {
				t = tp;
				nextCol = {at: t, A: A, B: B};
			}
		}

		return nextCol;
	}


	function detectBC(A, nextCol) {
		var t = nextCol.at;
		var tp = Infinity;  // t prime

		if (A.vx < 0) {  // moving left
			tp = (0 - A.rx + A.r) / A.vx;

			if (tp > 0 && tp < t && tp <= A.rem) {
				t = tp;
				nextCol = {at: t, A: A, dir: Dir.E};
			}
		} else {  // moving right
			tp = (w - A.rx - A.r) / A.vx;

			if (tp > 0 && tp < t && tp <= A.rem) {
				t = tp;
				nextCol = {at: t, A: A, dir: Dir.W};
			}
		}

		if (A.vy < 0) {  // moving up
			tp = (0 - A.ry + A.r) / A.vy;

			if (tp > 0 && tp < t) {
				t = tp;
				nextCol = {at: t, A: A, dir: Dir.N};
			}
		} else {  // moving down
			tp = (h - A.ry - A.r) / A.vy;

			if (tp > 0 && tp < t) {
				t = tp;
				nextCol = {at: t, A: A, dir: Dir.S};
			}
		}

		return nextCol;
	}


	function resolveBB(A, B, at) {
		// move balls to point of collision
		//
		// The accuracy of this is restricted by the accuracy of floating-point arithmetic
		// so if balls are overlapping we need to iteratively roll back the collision until
		// the balls are not intersecting.
		// TODO: Investigate more efficient ways of doing this
		var Arx, Ary, Brx, Bry;
		var dsq, rsq = Math.pow(A.r + B.r, 2);
		var K = 1 - 1e-4;

		do {
			Arx = A.rx + A.vx * at;
			Ary = A.ry + A.vy * at;

			Brx = B.rx + B.vx * at;
			Bry = B.ry + B.vy * at;

			at *= K;
			dsq = Math.pow(Arx - Brx, 2) + Math.pow(Ary - Bry, 2);
		} while (dsq < rsq);

		// actually move the balls to point of collision
		A.rx = Arx;
		A.ry = Ary;
		B.rx = Brx;
		B.ry = Bry;

		// work out velocities of balls after collision
		var r = A.r + B.r;
		// unit normal to collision
		var nx = (B.rx - A.rx) / r;
		var ny = (B.ry - A.ry) / r;

		// unit tangent to collision
		var tx = -ny;
		var ty = nx;

		// velocity parallel to line of centers of A and B
		var na = (A.vx * nx + A.vy * ny) * Pool.Collisions.E.BB;
		var nb = (B.vx * nx + B.vy * ny) * Pool.Collisions.E.BB;

		// velocity parallel to tangent
		var ta = A.vx * tx + A.vy * ty;
		var tb = B.vx * tx + B.vy * ty;


		// prepare elastic collision
		//  - velocities parallel to tangent are unchanged
		//  - velocities parallel to normal are swapped
		A.setV(nb*nx + ta*tx, nb*ny + ta*ty);
		B.setV(na*nx + tb*tx, na*ny + tb*ty);

		A.rem -= at;
		B.rem -= at;

		A.highlight();
		B.highlight();
	}


	function resolveBC(A, at, dir) {
		// move ball to cushion
		A.rx += A.vx * at;
		A.ry += A.vy * at;

		// resolve collision
		if(dir === Dir.N || dir === Dir.S) {

			// pot ball
			if (A.rx < 2*A.r || A.rx > w - 2*A.r || Math.pow(A.rx - w/2, 2) < A.r*A.r) {
				var idx = balls.indexOf(A);
				balls.splice(idx, 1);
			}

			A.vy *= -1;
		} else A.vx *= -1;  // E or W

		A.setV(A.vx * Pool.Collisions.E.BC, A.vy * Pool.Collisions.E.BC);
		A.rem -= at;
	}


	function resolver(dt) {

		var nextBC = {at: Infinity};  // next ball-cushion collision
		var nextBB = {at: Infinity};  // next ball-ball collision

		// detect collisions
		for (var i=0, A; i<balls.length; ++i) {
			A = balls[i];
			nextBC = detectBC(A, nextBC);
			nextBB = detectBB(A, nextBB);
		}


		// exit condition: no more collisions to resolve in this step
		if (nextBB.at > dt && nextBC.at > dt) return;


		// resolve collisions
		if (nextBB.at < nextBC.at) {
			resolveBB(nextBB.A, nextBB.B, nextBB.at);
		} else {
			resolveBC(nextBC.A, nextBC.at, nextBC.dir);
		}

		resolver(dt, balls, w, h);
	}

	return resolver(dt);
};
